package chapter_4;

public class NestedLoop_2 {

	public static void main(String[] args) {
		
		/*
		 *	 				*
		 * 				*	*
		 * 			*	*	*
		 * 		*	*	*	*	
		 * *	*	*	*	*
		 * 
		 * */
		
		/*
		 * @@@@@
		 * 
		 * @@@@@
		 * 
		 * @@@@@
		 * 
		 * @@@@@
		 * 
		 * @@@@@
		 * 
		 * @@@@@
		 *
		 * @@@@@
		 * 
		 * @@@@@
		 * 
		 * @@@@@
		 *
		 */

		int i, j, k;
		for (i = 1; i <= 3; i++) {
			for(j = 1; j <= 3; j++) {
				for(k = 1; k <= 5; k++) {
					System.out.print("@");
				}
				System.out.println();
			}
			System.out.println();
		}
		
		
		
		
		
		
		
		
		
		
		

	}
}
