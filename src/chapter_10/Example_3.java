package chapter_10;

abstract class Figure {
	double width;
	double length;

	public Figure(double width, double length) {
		this.width = width;
		this.length = length;
	}

	public void display() {
		System.out.println("Width = " + this.width);
		System.out.println("Length = " + this.length);
	}

	public abstract double area();
}

class Rectangle_1 extends Figure {

	public Rectangle_1(double width, double length) {
		super(width, length);
	}

	public void display() {
		super.display();
	}

	public double area() {
		return this.width * this.length;
	}

}

class Triangle extends Figure {

	public Triangle(double width, double length) {
		super(width, length);
	}

	public void display() {
		super.display();
	}

	public double area() {
		return 0.5 * this.width * this.length;
	}

}

public class Example_3 {

	public static void main(String[] args) {
		
		Rectangle_1 rectangle = new Rectangle_1(3.9, 1.5);
		rectangle.display();
		
		System.out.println("Rectangle Area = " + rectangle.area());
	}
	
}

/*
 * Rectangle (width, length) -> area() Triangle (width, length) -> area()
 * 
 */