package chapter_10;

class Person {
	protected String name;
	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}

class Student extends Person {
	private int mark;

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

}

class Employee extends Person {
	private int salary;

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

}

public class Example_1 {

	public static void main(String[] args) {
		Student student = new Student();
		
		student.setName("John");
		student.setAddress("Yangon");
		student.setMark(70);
		
		System.out.println(student.getName());
		System.out.println(student.getAddress());
		System.out.println(student.getMark());
		
		System.out.println("=========================");
		
		Employee employee = new Employee();
		
		employee.setName("John");
		employee.setAddress("Yangon");
		employee.setSalary(7000);
		
		System.out.println(employee.getName());
		System.out.println(employee.getAddress());
		System.out.println(employee.getSalary());
		
	}
}

/*
 * Person (name, address) Student (mark) Employee (salary)
 */
