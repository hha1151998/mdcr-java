package chapter_14;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class Guess {
	int hit, blow;
	Vector v1, v2;

	public Guess() {
		// TODO Auto-generated constructor stub
		this.hit = 0;
		this.blow = 0;
		this.v1 = new Vector();
		this.v2 = new Vector();

	}

	public Vector start() {

		Random r = new Random();
		String s;
		while (this.v1.size() < 4) {
			s = String.valueOf(r.nextInt(9) + 1);
			if (!this.v1.contains(s)) {
				this.v1.addElement(s);
			}

		}
		return this.v1;

	}

	public boolean play(Vector v1,Vector v2) {

		
		// display(v1);
		if (compareVector(v1,v2))
			return true;
		else
			return false;
	}

	public Vector requestUserInput() {

		boolean duplicate = false;
		boolean control = false;

		Vector v = new Vector();
		String st = "";

		Scanner scanner = new Scanner(System.in);

		while (!control) {
			v.clear();
			this.v2.clear();
			st = "";

			while (st.length() != 4) {
				System.out.print("Enter Number :");
				st = scanner.next();
			}

			for (int i = 0; i < st.length(); i++) {
				this.v2.addElement(String.valueOf(st.charAt(i))); // seperate string to vector

			}

			for (int i = 0; i < this.v2.size(); i++) { // test duplicate
				if (v.contains(this.v2.get(i)))
					duplicate = true;
				else
					v.add(this.v2.get(i));

			}

			if (duplicate) { // show duplicate
				System.out.println("Duplicate!Enter Again");
				duplicate = false;

			} else
				control = true;

		}
		return this.v2;
	}

	public boolean compareVector(Vector v1, Vector v2) {

		this.hit = 0;
		this.blow = 0;

		if (v1.equals(v2))
			return true;

		for (int i = 0; i < v1.size(); i++) {

			if (v1.get(i).equals(v2.get(i)))
				hit++;

			for (int j = 0; j < v1.size(); j++) {
				if (i != j && v1.get(i).equals(v2.get(j)))
					blow++;

			}

		}

		System.out.println("Result :" + hit + "H," + blow + "B");

		return false;
	}

	public void display(Vector v) {
		System.out.println(v);
	}

	public void showResult(boolean win) {

		if (win)
			System.out.println("!!!!!!!! YOU WIN !!!!!!!!!!!!!");
		else
			System.out.println("you lose");

	}

}
