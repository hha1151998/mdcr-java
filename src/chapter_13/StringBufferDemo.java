package chapter_13;

public class StringBufferDemo {

	public static void main(String[] args) {
//		String s1 = new String();

		char[] ch = { 'a', 'b', 'c', 'd', 'e' };

		String s2 = new String(ch);

		System.out.println(s2);

		String s3 = new String(ch, 0, 3);

		System.out.println(s3);

		String s4 = new String(s2);
		System.out.println(s4);

		char[] byteCodes = { 65, 66, 67, 68, 69 };
		
		String s5 = new String(byteCodes);
		
		System.out.println(s5);
		
		int i = 25;
		
		String num_3 =  String.valueOf(i);
		
		String str = "Hello";
		
		System.out.println(str.lastIndexOf('l'));
		
		System.out.println(str.substring(1, 3));
		
		System.out.println(str.substring(2));

	}
}
