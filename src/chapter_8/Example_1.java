package chapter_8;

import java.util.Scanner;

class Room {
	int width; // Instance Variable
	int length;
	Scanner scanner = new Scanner(System.in);
	
	public Room() { // default constructor
		this.width = 0;
		this.length = 0;
	}
	
	public Room(int width, int length) { // arguments constructor
		
	}

	public void setWidth() {
		this.width = scanner.nextInt();
	}

	public void setLength() {
		this.length = scanner.nextInt();
	}

	public int getWidth() {
		return this.width;
	}

	public int getLength() {
		return this.length;
	}

	public int area() {
		return this.width * this.length;
	}

	public void add(Room r1, Room r2) {
		this.width = r1.width + r2.width;
		this.length = r1.length + r2.length;
	}
}

public class Example_1 {

	public static void main(String[] args) {

		Room r1 = new Room();
		Room r2 = new Room();
		Room r3 = new Room();
		Room r4 = new Room();

		System.out.print("Enter Room1 width : ");
		r1.setWidth();
		System.out.print("Enter Room1 length : ");
		r1.setLength();

		System.out.println("Room1 width = " + r1.getWidth());
		System.out.println("Room1 length = " + r1.getLength());

		System.out.println("Room1 Area = " + r1.area());

		r4.add(r1, r2);
		System.out.println("Room4 Area = " + r4.area());
	}
}
