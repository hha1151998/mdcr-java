package chapter_5;

import java.util.Scanner;

public class Example_1 {

	public static void main(String[] args) {
		int [] A = new int[5];
		
		// 0   		1    	2    	3     	4  positions
		// A[0]		A[1]	A[2]	A[3]	A[4]  Values
		
		Scanner scanner = new Scanner(System.in);
		int i, sum = 0, max, min, avg, searchNumber;
		
		for(i = 0; i < A.length; i++) {
			System.out.print("Enter number: ");
			A[i] = scanner.nextInt();
			sum += A[i];
		}
		
		System.out.print("Enter Search Number: ");
		searchNumber = scanner.nextInt();
		
		boolean found = false;
		
		for(i = 0; i < A.length; i++) {
			if(searchNumber == A[i]) {
				System.out.println("Found at " + i);
				found = true;
			}
		}
		
		if(!found) {
			System.out.println("Not found");
		}
		
		
		
		
		
		
		
	}
}
