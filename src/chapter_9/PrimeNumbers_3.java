package chapter_9;

public class PrimeNumbers_3 {

	public static void main(String[] args) {
		System.out.println("Prime numbers from 2 to 1000");

		for (int i = 2; i < 1000; i++) {
			boolean isPrime = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					isPrime = false;
				}
			}

			if (isPrime) {
				System.out.print(i + "\t");
			}
		}
	}
}
