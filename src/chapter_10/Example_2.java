package chapter_10;

class Rectangle {
	private int width;
	private int height;

	public Rectangle() {
		this.width = 3;
		this.height = 9;
	}

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void display() {
		System.out.println("Width = " + this.width);
		System.out.println("Height = " + this.height);
	}

	public int area() {
		return this.height * this.width;
	}
}

class Cube extends Rectangle {
	private int length;

	public Cube() {
		super();
		this.length = 5;
	}

	public Cube(int width, int height, int length) {
		super(width, height);
		this.length = length;
	}

	public void display() {
		super.display();
		System.out.println("Length = " + this.length);
	}

	public int volumne() {
		return super.area() * this.length;
	}

}

public class Example_2 {

	public static void main(String[] args) {

		Rectangle rectangle = new Rectangle();
		rectangle.display();
		System.out.println("Rectangle Area = " + rectangle.area());

		Cube cube = new Cube();
		cube.display();
		System.out.println("Cube Volume = " + cube.volumne());
	}
}

/*
 * Rectangle (width, height) Cube (width, height, length)
 */