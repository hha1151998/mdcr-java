package chapter_14;

import java.util.Scanner;
import java.util.Vector;

public class GuessTest {

	public static void main(String[] args) {
		int count;

		Scanner scanner = new Scanner(System.in);

		boolean exit = false;
		boolean win;

		Vector v1 = new Vector();
		Vector v2 = new Vector();

		while (!exit) {
			win = false;
			count = 1;
			Guess g = new Guess();
			v1 = g.start();
			g.display(g.v1);

			while (!win && count <= 5) {

				System.out.println("\n" + count + " time");
				v2 = g.requestUserInput();
				if (g.play(v1, v2))
					win = true;
				else
					count++;

			}

			g.showResult(win);

			System.out.println("Enter 0 for Exit (or) 1 for try again:");
			int num = scanner.nextInt();
			exit = (num == 0) ? true : false;

		}

		System.out.println("Thank for Playing Guess Game!!!Bye");

	}

}
