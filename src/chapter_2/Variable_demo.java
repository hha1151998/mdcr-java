package chapter_2;

public class Variable_demo {
	public static void main(String[] args) {
		int a = 10, b = 11;
		int c, d;
		c = 12;
		d = 19;
		
		byte _byte = 22;
		short _short = 223;
		long _long = 434;
		char _char = 'A';
		
		float _float = 34.3f;
		double _double = 3434.3434;
		
		String name = null;		
		String country = new String("US");
		
		boolean flag = true; // false
		
		System.out.println(country);
		System.out.println("flag = " + flag);
		
		System.out.println(name);
	}
}
