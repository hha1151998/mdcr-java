package chapter_14;

public class ExceptionHandling {

	public static void main(String[] args) {
		int d, a;

		d = 0;

		try {
			a = 42 / d;

			System.out.println("A = " + a);

		} catch (ArithmeticException e) {
			System.out.println("Division by zero");
		}
		
		System.out.println("After catch statement");
		
		

	}
}
