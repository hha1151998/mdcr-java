package chapter_12.pack;

public class ArrayTest {

	public static int sum(int[] a) {
		int total = 0;
		for(int i = 0; i < a.length; i++) {
			total += a[i];
		}
		
		return total;
	}
}
