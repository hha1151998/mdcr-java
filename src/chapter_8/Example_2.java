package chapter_8;

class Room_Con {
	int width;
	int length;

	public Room_Con() {
		this.width = 10;
		this.length = 20;
	}

	public Room_Con(int w, int l) {
		this.width = w;
		this.length = l;
	}

	public void add(Room_Con r2) {
		this.width += r2.width;
		this.length += r2.length;
	}

	public void display() {
		System.out.println("Width = " + this.width);
		System.out.println("Length = " + this.length);
	}
}

public class Example_2 {
	public static void main(String[] args) {
		Room_Con r1 = new Room_Con(); // default constructor
		Room_Con r2 = new Room_Con(35, 40); // Arguments constructor
		
		System.out.println("=== Before Add =====");
		r1.display();
		r2.display();

		System.out.println("=== After Add =====");
		r1.add(r2);
		r1.display();
	}
}
