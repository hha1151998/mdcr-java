package chapter_4;

public class LoopDemo {
	public static void main(String[] args) {
		int i, n = 10;

		for (i = 1; i < n; i += 3) {
			System.out.print(i + "\t");
		}
		
		System.out.println("After Loop");
		
		System.out.println(i);
	}
}
