package hitAndBlow;

public class NumberExceededException extends Exception {
	private String errorMassage;

	public NumberExceededException() {
		this.errorMassage = "\nInput should be four numbers without spaces!!\n\n";
	}
	
	public String toString() {
		return this.errorMassage;
	}
}
