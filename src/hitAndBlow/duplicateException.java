package hitAndBlow;

public class duplicateException extends Exception {

	private String errorMassage;

	public duplicateException() {
		this.errorMassage = "Duplicate number detected!\nTry again";
	}
	
	public String toString() {
		return this.errorMassage;
	}
}
