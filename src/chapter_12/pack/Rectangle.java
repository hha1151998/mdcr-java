package chapter_12.pack;

public class Rectangle {
	public int width, height;

	public Rectangle(int w, int h) {
		width = w;
		height = h;
	}

	public void CalculateArea() {
		System.out.println("Rectangle Area=" + width * height);
	}
}
