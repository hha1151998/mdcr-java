package chapter_12;

import chapter_12.pack.ArrayTest;
import chapter_12.pack.Rectangle;
import chapter_12.pack.Triangle;


public class TestPackage {
	public static void main(String[] args) {
		
		Rectangle r = new Rectangle(5, 10);
		r.CalculateArea();
		
		Triangle t = new Triangle(12, 7);
		System.out.println(t.CalculateArea());
		
		System.out.println(ArrayTest.sum(new int[]{1, 2, 3}));
	}
}
