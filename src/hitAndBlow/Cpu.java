package hitAndBlow;

import java.util.Random;
import java.util.Vector;

public class Cpu {

	private Vector<String> genNum;
	Random ran = new Random();
	
	public Cpu() {
		// TODO Auto-generated constructor stub
		this.genNum = new Vector<>();
	}
	
	public Vector<String> generateNumbers() {
		Vector<String> v = new Vector<>();
		while(v.size() < 4) {
			String n = Integer.toString(ran.nextInt(9) + 1);
			if (!v.contains(n)) {
				v.add(n);
			}
		}
		return v;
	}
	
	public boolean compare(Vector<String> user) {
		int hit = 0;
		int blow = 0;
		
		for (int i = 0; i < this.genNum.size(); i++) {
			if (user.get(i).equals(this.genNum.get(i))) {
				hit++;
			} else if (this.genNum.contains(user.get(i))) {
				blow++;
			}
		}
		
		if (hit == 4) {
			System.out.println("\n" + hit + " Hit and " + blow + " Blow\n");
			System.out.println("You Won!!");
			return true;
		}
		System.out.println("\n" + hit + " Hit and " + blow + " Blow\n");
		hit = 0;
		blow = 0;
		return false;
	}

	public Vector<String> getGenNum() {
		return genNum;
	}

	public void setGenNum(Vector<String> genNum) {
		this.genNum = genNum;
	}

	public Random getRan() {
		return ran;
	}

	public void setRan(Random ran) {
		this.ran = ran;
	}
	
	
}
